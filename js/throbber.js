/**
 * @file
 * Replaced Drupal cores ajax throbber(s), see: https://www.drupal.org/node/2974681
 *
 */

Drupal.theme.ajaxProgressThrobber = function () {
  return (
    '<div class="ajax-spinner ajax-spinner--inline d-inline-block"><div class="spinner-border"></div><span class="ajax-spinner__label visually-hidden">' +
    Drupal.t(
      "Loading&nbsp;&hellip;",
      {},
      { context: "Loading text for Drupal cores Ajax throbber (inline)" }
    ) +
    "</span></div>"
  );
};

// Drupal.theme.ajaxProgressIndicatorFullscreen = function () {
//   return "<div class=\"ajax-spinner ajax-spinner--fullscreen\"><span class=\"ajax-spinner__label\">" + Drupal.t('Loading&nbsp;&hellip;', {}, {
//     context: "Loading text for Drupal cores Ajax throbber (fullscreen)"
//   }) + "</span></div>";
// };
